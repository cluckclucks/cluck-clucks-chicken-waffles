A fun - family-friendly brand, dedicated to everything fried chicken and waffles. Based in Toronto, Ontario, Cluck Clucks has been at the forefront of converting an exotic food combination into comfort food. 

Cluck Clucks operates in an open kitchen, so patrons can see their food being made right in front of them.

Website : https://www.cluckclucks.ca/